package nl.utwente.soa.sampleWebapplication.services;

import nl.utwente.soa.sampleWebapplication.domain.Movie;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class StaticMovieService implements MovieService {

    public List<Movie> getMovies(){
        return Arrays.asList(
            new Movie("SOA with webservices","2024", 
            		Arrays.asList("Leon", "João)"), "About service-oriented architecture with webservices", "EN"),
            new Movie("SOA with webservices","2024", 
            		Arrays.asList("Leon", "João)"), "About microservices", "EN")
        );
    }
}
